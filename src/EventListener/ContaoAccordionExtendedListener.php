<?php

namespace designerei\ContaoAccordionExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ContaoAccordionExtendedListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        $arrCTE = array(
            'ce_accordionSingle',
            'ce_accordionStart'
        );

        foreach ($arrCTE as $CTE) {
            if ($CTE === $template->getName()) {
                $accordionHeadline = deserialize($template->accordionHeadline);
                $template->headline = $accordionHeadline['value'];
                $template->headlineClass = $template->accordionHeadlineClass;
                $template->tag = $accordionHeadline['unit'];
            }
        }
    }
}

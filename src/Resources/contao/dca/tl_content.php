<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['fields']['accordionHeadline'] = [
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'inputUnit',
    'options'                 => array('p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'),
    'eval'                    => array('mandatory'=>true, 'maxlength'=>200, 'tl_class'=>'clr w50', 'allowHtml'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['accordionHeadlineClass'] = [
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];


PaletteManipulator::create()
  ->addField('accordionHeadline', 'moo_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('accordionHeadlineClass', 'moo_legend', PaletteManipulator::POSITION_APPEND)
  ->removeField('mooHeadline', 'moo_legend')
  ->removeField('mooClasses', 'moo_legend')
  ->removeField('mooStyle', 'moo_legend')
  ->applyToPalette('accordionSingle', 'tl_content')
  ->applyToPalette('accordionStart', 'tl_content')
;
